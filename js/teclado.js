var SETA_ESQUERDA = 37;
var SETA_DIREITA = 39;
var ESPACO = 32;


function Teclado(element){
	this.element = element;

	this.pressionadas = [];

	this.disparadas = [];

	this.funcoesDisparo = [];

	var teclado = this;

		element.addEventListener('keydown', function(event){
		var tecla = event.keyCode;
		teclado.pressionadas[tecla] = true;
	

	if(teclado.funcoesDisparo[tecla] && ! teclado.disparadas[tecla]){

		teclado.disparadas[tecla] = true;
		teclado.funcoesDisparo[tecla] ();
		}
	});

	element.addEventListener('keyup', function(event){
		teclado.pressionadas[event.keyCode] = false;
		teclado.disparadas[event.keyCode] = false;
	});

}
Teclado.prototype = {
	pressionada: function(tecla){
		return this.pressionadas[tecla];
	},
	
	disparou: function(tecla, callback){
		this.funcoesDisparo[tecla] = callback;
	}
};

	